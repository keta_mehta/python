

l1 = [x for x in range(1,11)]
print("l1 =",l1)

l2 = [x for x in range(10,112,10)]
print("l2 =",l2)

l3 = ['python','django','flask','string','function','classes']
print("l3 =",l3)

l4 = {"l1":l1,"l2":l2,"l3":l3}
print("l4 =",l4)

main_list = []

main_list.append(l1)
main_list.append(l2)
main_list.append(l3)

print("main_list incuding l1,l2,l3 ")
print("main_list:",main_list)

l5 = []
l5 = (l1*20)

print("l5 =",l5)

main_list.append(l5)

print("main_list after appending l5: ")
print("main_list:",main_list)
